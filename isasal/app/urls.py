from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    url('index/', views.index, name='index'),
    url('admin/', admin.site.urls),
    url('registro/', views.registro, name='registro'),
#    url('login/', views.login, name='login'),
    url('perfil/', views.perfil, name='perfil'),
    url('categorias/', views.categorias, name='categorias'),
    url('ubicaciones/', views.ubicaciones, name='ubicaciones'),
]
