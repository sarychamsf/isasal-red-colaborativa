from django.db import models

# Create your models here.
from django.db import models

class Usuario(models.Model):
    nombre = models.CharField(max_length=70)
    usuario = models.CharField(max_length=20)
    email = models.CharField(max_length=30)
    clave = models.CharField(max_length=15)
    def __str__(self):
        return self.usuario