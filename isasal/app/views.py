from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Usuario
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    return render(request, 'app/index.html')

def registro(request):
    if (request.method == 'POST'):
        nombre = request.POST['nombre']
        email = request.POST['email']
        usuario = request.POST['usuario']
        clave = request.POST['clave']
        new_usuario = Usuario(nombre=nombre, email=email, usuario=usuario, clave=clave)
        new_usuario.save()
        return redirect('/login')
    return render(request, 'registro/registro.html')

@login_required
def perfil(request):
    usuario = request.user
    return render(request, 'perfil/perfil.html', {'usuario': usuario})

@login_required
def categorias(request):
    return render(request, 'categorias/categorias.html')

@login_required
def ubicaciones(request):
    return render(request, 'ubicaciones/ubicaciones.html')