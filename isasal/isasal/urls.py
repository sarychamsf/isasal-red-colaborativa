from django.conf.urls import include, url
from django.urls import path

urlpatterns = [
    url('', include('app.urls')),
]

# Add Django site authentication urls (for login, logout, password management)
urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls')),
]